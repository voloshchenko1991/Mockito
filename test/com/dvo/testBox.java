package com.dvo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
/**
 * Created by java on 02.08.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class testBox {
    @Mock
    Cat cat;
    @InjectMocks
    Box box;

    @Test
    public void testBox(){
        when(cat.isAlive(20)).thenReturn(true);
        when(cat.isAlive(50)).thenReturn(false);
        Assert.assertEquals("meow",box.open(20));
        verify(cat,times(1)).isAlive(anyInt());
        Assert.assertEquals("smell",box.open(50));

    }
}
