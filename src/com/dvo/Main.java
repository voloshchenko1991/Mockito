package com.dvo;

public class Main {

    public static void main(String[] args) {
        Cat cat = new Cat();
	    Box box = new Box(cat);
	    box.open(20);
    }
}
