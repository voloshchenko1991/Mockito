package com.dvo;

/**
 * Created by java on 02.08.2017.
 */
public class Box {
    private Cat cat;
    Box(Cat cat){
        this.cat = cat;
    }
    public String open(int hours){
       return cat.isAlive(hours)?"meow":"smell";
    }
}
